"""txn description

Revision ID: b4cda575e06a
Revises: 53743edb5706
Create Date: 2024-06-30 14:47:00.635067

"""

from typing import Sequence, Union

import sqlalchemy as sa
import sqlmodel
from alembic import op

# revision identifiers, used by Alembic.
revision: str = "b4cda575e06a"
down_revision: Union[str, None] = "53743edb5706"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column(
        "transaction",
        sa.Column("description", sqlmodel.sql.sqltypes.AutoString(), nullable=True),
    )


def downgrade() -> None:
    op.drop_column("transaction", "description")
