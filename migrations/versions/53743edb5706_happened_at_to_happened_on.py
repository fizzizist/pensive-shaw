"""happened_at to happened_on

Revision ID: 53743edb5706
Revises: a28a561b31fb
Create Date: 2024-06-30 14:23:01.350921

"""

from typing import Sequence, Union

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql
from sqlmodel import func, text

# revision identifiers, used by Alembic.
revision: str = "53743edb5706"
down_revision: Union[str, None] = "a28a561b31fb"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column(
        "transaction",
        sa.Column(
            "happened_on",
            sa.Date(),
            nullable=False,
            server_default=func.date(func.now()),
        ),
    )
    conn = op.get_bind()
    conn.execute(text("update transaction set happened_on = happened_at::date;"))
    op.drop_column("transaction", "happened_at")


def downgrade() -> None:
    op.add_column(
        "transaction",
        sa.Column(
            "happened_at",
            postgresql.TIMESTAMP(),
            autoincrement=False,
            nullable=False,
            server_default=func.now(),
        ),
    )
    conn = op.get_bind()
    conn.execute(text("update transaction set happened_at = happened_on::timestamp;"))
    op.drop_column("transaction", "happened_on")
