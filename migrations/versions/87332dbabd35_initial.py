"""initial

Revision ID: 87332dbabd35
Revises:
Create Date: 2024-06-16 15:46:18.324916

"""

from typing import Sequence, Union

import sqlalchemy as sa
import sqlmodel
from alembic import op

# revision identifiers, used by Alembic.
revision: str = "87332dbabd35"
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        "book",
        sa.Column("id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("name", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("name"),
    )
    op.create_index(op.f("ix_book_id"), "book", ["id"], unique=False)
    op.create_table(
        "currency",
        sa.Column("id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column(
            "currency_type",
            sa.Enum("STOCK", "CRYPTO", "WORLD", name="currencytype"),
            nullable=False,
        ),
        sa.Column("symbol", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint(
            "symbol", "currency_type", name="symbol_currency_type_unique"
        ),
    )
    op.create_index(op.f("ix_currency_id"), "currency", ["id"], unique=False)
    op.create_table(
        "price",
        sa.Column("id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("amount", sa.Numeric(precision=5, scale=3), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_price_id"), "price", ["id"], unique=False)
    op.create_table(
        "account",
        sa.Column("id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("name", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("description", sqlmodel.sql.sqltypes.AutoString(), nullable=True),
        sa.Column("parent_id", sqlmodel.sql.sqltypes.GUID(), nullable=True),
        sa.Column("currency_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("book_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column(
            "acc_type",
            sa.Enum("ASSET", "LIABILITY", name="accounttype"),
            nullable=False,
        ),
        sa.ForeignKeyConstraint(
            ["book_id"],
            ["book.id"],
        ),
        sa.ForeignKeyConstraint(
            ["currency_id"],
            ["currency.id"],
        ),
        sa.ForeignKeyConstraint(
            ["parent_id"],
            ["account.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_account_book_id"), "account", ["book_id"], unique=False)
    op.create_index(
        op.f("ix_account_currency_id"), "account", ["currency_id"], unique=False
    )
    op.create_index(op.f("ix_account_id"), "account", ["id"], unique=False)
    op.create_index(
        op.f("ix_account_parent_id"), "account", ["parent_id"], unique=False
    )
    op.create_table(
        "transaction",
        sa.Column("id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("account_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("amount", sa.Numeric(precision=5, scale=3), nullable=False),
        sa.Column("new_balance", sa.Numeric(precision=5, scale=3), nullable=False),
        sa.Column(
            "txn_type",
            sa.Enum("CREDIT", "DEBIT", name="transactiontype"),
            nullable=False,
        ),
        sa.Column("happened_at", sa.DateTime(), nullable=False),
        sa.Column("reconciled", sa.Boolean(), nullable=False),
        sa.ForeignKeyConstraint(
            ["account_id"],
            ["account.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(
        op.f("ix_transaction_account_id"), "transaction", ["account_id"], unique=False
    )
    op.create_index(op.f("ix_transaction_id"), "transaction", ["id"], unique=False)
    op.create_index(
        op.f("ix_transaction_txn_type"), "transaction", ["txn_type"], unique=False
    )


def downgrade() -> None:
    op.drop_index(op.f("ix_transaction_txn_type"), table_name="transaction")
    op.drop_index(op.f("ix_transaction_id"), table_name="transaction")
    op.drop_index(op.f("ix_transaction_account_id"), table_name="transaction")
    op.drop_table("transaction")
    op.drop_index(op.f("ix_account_parent_id"), table_name="account")
    op.drop_index(op.f("ix_account_id"), table_name="account")
    op.drop_index(op.f("ix_account_currency_id"), table_name="account")
    op.drop_index(op.f("ix_account_book_id"), table_name="account")
    op.drop_table("account")
    op.drop_index(op.f("ix_price_id"), table_name="price")
    op.drop_table("price")
    op.drop_index(op.f("ix_currency_id"), table_name="currency")
    op.drop_table("currency")
    op.drop_index(op.f("ix_book_id"), table_name="book")
    op.drop_table("book")
