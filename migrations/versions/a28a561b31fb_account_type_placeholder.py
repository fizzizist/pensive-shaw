"""account type placeholder

Revision ID: a28a561b31fb
Revises: 61d3b95c0089
Create Date: 2024-06-29 15:36:23.368627

"""

from typing import Sequence, Union

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision: str = "a28a561b31fb"
down_revision: Union[str, None] = "61d3b95c0089"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    conn = op.get_bind()
    conn.execute(sa.text("ALTER type accounttype ADD VALUE 'PLACEHOLDER';"))


def downgrade() -> None:
    # you cannot drop enum types this revision is not backward compatible
    raise RuntimeError("This revision is backwards-incompatible")
