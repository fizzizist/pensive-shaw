"""decimal places

Revision ID: 61d3b95c0089
Revises: 6e1508b8f5de
Create Date: 2024-06-22 22:03:03.015072

"""

from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = "61d3b95c0089"
down_revision: Union[str, None] = "6e1508b8f5de"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.alter_column("transaction", "amount", type_=sa.Numeric())
    op.alter_column("price", "amount", type_=sa.Numeric())


def downgrade() -> None:
    op.alter_column("transaction", "amount", type_=sa.Numeric(precision=5, scale=2))
    op.alter_column("price", "amount", type_=sa.Numeric(precision=5, scale=2))
