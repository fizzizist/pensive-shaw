"""paired account on txn

Revision ID: a8774e11ef83
Revises: b4cda575e06a
Create Date: 2024-06-30 23:55:41.020109

"""

from typing import Sequence, Union

import sqlalchemy as sa
import sqlmodel
from alembic import op
from sqlmodel import text

# revision identifiers, used by Alembic.
revision: str = "a8774e11ef83"
down_revision: Union[str, None] = "b4cda575e06a"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column(
        "transaction",
        sa.Column("paired_account_id", sqlmodel.sql.sqltypes.GUID(), nullable=True),
    )
    conn = op.get_bind()
    conn.execute(
        text("""
        UPDATE transaction txn
        SET paired_account_id = pacc.id
        FROM account pacc, transactionpair tp, transaction txn_p
        WHERE
            tp.debit_txn_id = txn.id
            and txn_p.id = tp.credit_txn_id
            and txn_p.account_id = pacc.id;
            """)
    )
    conn.execute(
        text("""
        UPDATE transaction txn
        SET paired_account_id = pacc.id
        FROM account pacc, transactionpair tp, transaction txn_p
        WHERE
            tp.credit_txn_id = txn.id
            and txn_p.id = tp.debit_txn_id
            and txn_p.account_id = pacc.id;
            """)
    )
    op.alter_column("transaction", "paired_account_id", nullable=False)
    op.create_foreign_key(
        "txn_paired_acc_fk", "transaction", "account", ["paired_account_id"], ["id"]
    )


def downgrade() -> None:
    op.drop_constraint("txn_paired_acc_fk", "transaction", type_="foreignkey")
    op.drop_column("transaction", "paired_account_id")
