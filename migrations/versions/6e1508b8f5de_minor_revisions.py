"""minor revisions

Revision ID: 6e1508b8f5de
Revises: 87332dbabd35
Create Date: 2024-06-22 15:24:29.762824

"""

from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
import sqlmodel


# revision identifiers, used by Alembic.
revision: str = "6e1508b8f5de"
down_revision: Union[str, None] = "87332dbabd35"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        "transactionpair",
        sa.Column("id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("debit_txn_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("credit_txn_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.ForeignKeyConstraint(
            ["credit_txn_id"],
            ["transaction.id"],
        ),
        sa.ForeignKeyConstraint(
            ["debit_txn_id"],
            ["transaction.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(
        op.f("ix_transactionpair_credit_txn_id"),
        "transactionpair",
        ["credit_txn_id"],
        unique=True,
    )
    op.create_index(
        op.f("ix_transactionpair_debit_txn_id"),
        "transactionpair",
        ["debit_txn_id"],
        unique=True,
    )
    op.create_index(
        op.f("ix_transactionpair_id"), "transactionpair", ["id"], unique=False
    )
    op.add_column(
        "currency", sa.Column("name", sqlmodel.sql.sqltypes.AutoString(), nullable=True)
    )
    op.add_column(
        "currency",
        sa.Column("description", sqlmodel.sql.sqltypes.AutoString(), nullable=True),
    )
    op.add_column(
        "price", sa.Column("currency_id", sqlmodel.sql.sqltypes.GUID(), nullable=False)
    )
    op.create_index(
        op.f("ix_price_currency_id"), "price", ["currency_id"], unique=False
    )
    op.create_foreign_key(
        "price_currency_id_fk", "price", "currency", ["currency_id"], ["id"]
    )
    op.drop_column("transaction", "new_balance")
    # ### end Alembic commands ###


def downgrade() -> None:
    op.add_column(
        "transaction",
        sa.Column(
            "new_balance",
            sa.NUMERIC(precision=5, scale=3),
            autoincrement=False,
            nullable=False,
        ),
    )
    op.drop_constraint("price_currency_id_fk", "price", type_="foreignkey")
    op.drop_index(op.f("ix_price_currency_id"), table_name="price")
    op.drop_column("price", "currency_id")
    op.drop_column("currency", "description")
    op.drop_column("currency", "name")
    op.drop_index(op.f("ix_transactionpair_id"), table_name="transactionpair")
    op.drop_index(op.f("ix_transactionpair_debit_txn_id"), table_name="transactionpair")
    op.drop_index(
        op.f("ix_transactionpair_credit_txn_id"), table_name="transactionpair"
    )
    op.drop_table("transactionpair")
