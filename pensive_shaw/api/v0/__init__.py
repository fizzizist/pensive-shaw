from fastapi import APIRouter

from pensive_shaw.api.v0.book import router as book_router
from pensive_shaw.api.v0.account import router as account_router
from pensive_shaw.api.v0.currency import router as currency_router
from pensive_shaw.api.v0.transaction import router as txn_router

router = APIRouter(prefix="/v0")
router.include_router(book_router)
router.include_router(account_router)
router.include_router(currency_router)
router.include_router(txn_router)
