from fastapi import APIRouter, Depends, HTTPException
from pensive_shaw.models import Currency
from pensive_shaw.schemas.currency import CurrencyInput, CurrencyOutput, CurrencyUpdate
from pensive_shaw.services.currency import CurrencyService
from uuid import UUID

router = APIRouter(prefix="/currencies", tags=["currencies"])


@router.post("/", response_model=CurrencyOutput, status_code=201)
def create(curr: CurrencyInput, service: CurrencyService = Depends()) -> Currency:
    return service.create(curr)


@router.patch("/", response_model=CurrencyOutput, status_code=200)
def update(curr: CurrencyUpdate, service: CurrencyService = Depends()) -> Currency:
    try:
        return service.update(curr)
    except ValueError as e:
        raise HTTPException(status_code=404, detail=str(e))


@router.get("/", response_model=list[CurrencyOutput], status_code=200)
def index(service: CurrencyService = Depends()) -> list[Currency]:
    return service.list()


@router.get("/{currency_id}", response_model=CurrencyOutput, status_code=200)
def get(currency_id: UUID, service: CurrencyService = Depends()) -> Currency:
    try:
        return service.get(currency_id)
    except ValueError as e:
        raise HTTPException(status_code=404, detail=str(e))
