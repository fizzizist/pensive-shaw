from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException

from pensive_shaw.models import Account
from pensive_shaw.schemas.account import AccountInput, AccountOutput, AccountUpdate
from pensive_shaw.services.account import AccountService

router = APIRouter(prefix="/accounts", tags=["accounts"])


@router.post("/", response_model=AccountOutput, status_code=201)
def create(acc: AccountInput, service: AccountService = Depends()) -> Account:
    return service.create(acc)


@router.patch("/", response_model=AccountOutput, status_code=200)
def update(acc: AccountUpdate, service: AccountService = Depends()) -> Account:
    try:
        return service.update(acc)
    except ValueError as e:
        raise HTTPException(status_code=404, detail=str(e)) from e


@router.get("/", response_model=list[AccountOutput], status_code=200)
def index(service: AccountService = Depends()) -> list[Account]:
    return service.list()


@router.get("/{account_id}", response_model=AccountOutput, status_code=200)
def get(account_id: UUID, service: AccountService = Depends()) -> Account:
    try:
        return service.get(account_id)
    except ValueError as e:
        raise HTTPException(status_code=404, detail=str(e)) from e
