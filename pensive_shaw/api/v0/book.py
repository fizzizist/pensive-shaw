from fastapi import APIRouter, Depends, HTTPException
from pensive_shaw.models import Book
from pensive_shaw.schemas.book import BookInput
from pensive_shaw.services.book import BookService
from uuid import UUID

router = APIRouter(prefix="/books", tags=["books"])


@router.post("/", response_model=Book, status_code=201)
def create(book: BookInput, service: BookService = Depends()) -> Book:
    return service.create(book)


@router.patch("/", response_model=Book, status_code=200)
def update(book: Book, service: BookService = Depends()) -> Book:
    return service.update(book)


@router.get("/", response_model=list[Book], status_code=200)
def index(service: BookService = Depends()) -> list[Book]:
    return service.list()


@router.get("/{book_id}", response_model=Book, status_code=200)
def get(book_id: UUID, service: BookService = Depends()) -> Book:
    try:
        return service.get(book_id)
    except ValueError as e:
        HTTPException(status_code=404, detail=str(e))
