from fastapi import APIRouter, Depends, Query, HTTPException
from uuid import UUID
from pensive_shaw.schemas.transaction import (
    TransactionPairOutput,
    TransactionInput,
    TransactionOutput,
    TransactionUpdate,
)
from pensive_shaw.services.transaction import TransactionService
from pensive_shaw.models import TransactionPair
from pensive_shaw.models.transaction import Transaction, TransactionType
from typing import Annotated

router = APIRouter(prefix="/transactions", tags=["transactions"])


@router.post("/", response_model=TransactionPairOutput, status_code=201)
def create(
    txn: TransactionInput, service: TransactionService = Depends()
) -> TransactionPair:
    try:
        return service.create(txn)
    except ValueError as e:
        raise HTTPException(status_code=422, detail=str(e)) from e


@router.get("/{txn_id}", response_model=TransactionOutput, status_code=200)
def get(txn_id: UUID, service: TransactionService = Depends()) -> Transaction:
    try:
        return service.get(txn_id=txn_id)
    except ValueError as e:
        raise HTTPException(status_code=404, detail=str(e)) from e


@router.get("/", response_model=list[TransactionOutput], status_code=200)
def index(
    txn_type: Annotated[TransactionType | None, Query(alias="txn-type")] = None,
    account_id: Annotated[UUID | None, Query(alias="account-id")] = None,
    ids_in: Annotated[list[UUID] | None, Query(alias="ids-in")] = None,
    service: TransactionService = Depends(),
) -> list[Transaction]:
    return service.list(txn_type=txn_type, acc_id=account_id, ids=ids_in)


@router.delete("/{txn_id}", status_code=204)
def delete(txn_id: UUID, service: TransactionService = Depends()) -> None:
    try:
        return service.delete(txn_id=txn_id)
    except ValueError as e:
        raise HTTPException(status_code=404, detail=str(e)) from e


@router.patch("/", response_model=TransactionPairOutput, status_code=200)
def update(
    txn_update: TransactionUpdate, service: TransactionService = Depends()
) -> TransactionPair:
    return service.update(txn_update=txn_update)
