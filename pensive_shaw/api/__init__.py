from fastapi import APIRouter

from pensive_shaw.api.health import router as health_router
from pensive_shaw.api.v0 import router as v0_router

router = APIRouter(prefix="/api")
router.include_router(health_router)
router.include_router(v0_router)
