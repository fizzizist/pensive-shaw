from datetime import date
from decimal import Decimal
from enum import Enum
from typing import TYPE_CHECKING
from uuid import UUID

from sqlmodel import Date, Field, Relationship, SQLModel
from uuid6 import uuid7

if TYPE_CHECKING:
    from pensive_shaw.models import Account


class TransactionType(Enum):
    CREDIT = "CREDIT"
    DEBIT = "DEBIT"


class TransactionBase(SQLModel):
    """Represents a financial transaction from one account to another"""

    id: UUID = Field(default_factory=uuid7, primary_key=True, index=True)
    description: str | None
    account_id: UUID = Field(foreign_key="account.id", index=True)
    paired_account_id: UUID = Field(foreign_key="account.id")
    amount: Decimal
    txn_type: TransactionType = Field(index=True)
    happened_on: date = Field(sa_type=Date, default_factory=date.today)
    reconciled: bool = Field(default=False)


class Transaction(TransactionBase, table=True):
    """Represents a financial transaction from one account to another"""

    account: "Account" = Relationship(
        back_populates="transactions",
        sa_relationship_kwargs=dict(primaryjoin="Transaction.account_id == Account.id"),
    )
    paired_account: "Account" = Relationship(
        sa_relationship_kwargs=dict(
            primaryjoin="Transaction.paired_account_id == Account.id"
        ),
    )
