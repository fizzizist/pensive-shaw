from uuid import UUID

from sqlmodel import Field, SQLModel, Relationship
from uuid6 import uuid7
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from pensive_shaw.models import Transaction


class TransactionPairBase(SQLModel):
    id: UUID = Field(default_factory=uuid7, primary_key=True, index=True)
    debit_txn_id: UUID = Field(foreign_key="transaction.id", index=True, unique=True)
    credit_txn_id: UUID = Field(foreign_key="transaction.id", index=True, unique=True)


class TransactionPair(TransactionPairBase, table=True):
    debit_txn: "Transaction" = Relationship(
        sa_relationship_kwargs={
            "primaryjoin": "TransactionPair.debit_txn_id==Transaction.id",
            "lazy": "joined",
        },
    )
    credit_txn: "Transaction" = Relationship(
        sa_relationship_kwargs={
            "primaryjoin": "TransactionPair.credit_txn_id==Transaction.id",
            "lazy": "joined",
        },
    )
