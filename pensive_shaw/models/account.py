from enum import Enum
from typing import TYPE_CHECKING
from uuid import UUID

from sqlmodel import Field, Relationship, SQLModel
from uuid6 import uuid7

if TYPE_CHECKING:
    from pensive_shaw.models.book import Book
    from pensive_shaw.models.currency import Currency
    from pensive_shaw.models.transaction import Transaction


class AccountType(Enum):
    ASSET = "ASSET"
    LIABILITY = "LIABILITY"
    PLACEHOLDER = "PLACEHOLDER"


class AccountBase(SQLModel):
    """Represents a single account capable of performing transactions"""

    name: str
    description: str | None = Field(default=None)
    parent_id: UUID | None = Field(default=None, foreign_key="account.id", index=True)
    currency_id: UUID = Field(foreign_key="currency.id", index=True)
    book_id: UUID = Field(foreign_key="book.id", index=True)
    acc_type: AccountType


class Account(AccountBase, table=True):
    """Represents a single account capable of performing transactions"""

    id: UUID = Field(default_factory=uuid7, primary_key=True, index=True)
    transactions: list["Transaction"] = Relationship(
        back_populates="account",
        sa_relationship_kwargs=dict(primaryjoin="Transaction.account_id == Account.id"),
    )
    parent: "Account" = Relationship(
        back_populates="children",
        sa_relationship_kwargs=dict(remote_side="Account.id"),
    )
    children: list["Account"] = Relationship(back_populates="parent")
    currency: "Currency" = Relationship(back_populates="accounts")
    book: "Book" = Relationship(back_populates="accounts")
