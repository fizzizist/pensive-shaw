from typing import TYPE_CHECKING
from uuid import UUID
from sqlmodel import Field, Relationship, SQLModel
from uuid6 import uuid7

if TYPE_CHECKING:
    from pensive_shaw.models.account import Account


class Book(SQLModel, table=True):
    """Represents a Financial book containing accounts, transactions, etc."""

    id: UUID = Field(default_factory=uuid7, primary_key=True, index=True)
    name: str = Field(unique=True)
    accounts: list["Account"] = Relationship(back_populates="book")
