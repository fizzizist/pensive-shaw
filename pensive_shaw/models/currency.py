from enum import Enum
from typing import TYPE_CHECKING
from uuid import UUID

from sqlmodel import Field, Relationship, SQLModel, UniqueConstraint
from uuid6 import uuid7

if TYPE_CHECKING:
    from pensive_shaw.models.account import Account
    from pensive_shaw.models.price import Price


class CurrencyType(Enum):
    STOCK = "STOCK"
    CRYPTO = "CRYPTO"
    WORLD = "WORLD"


class CurrencyBase(SQLModel):
    """Represents a particular currency that acts as a value store"""

    currency_type: CurrencyType
    symbol: str
    name: str | None = Field(default=None)
    description: str | None = Field(default=None)


class Currency(CurrencyBase, table=True):
    """Represents a particular currency that acts as a value store"""

    __table_args__ = (
        UniqueConstraint("symbol", "currency_type", name="symbol_currency_type_unique"),
    )

    id: UUID = Field(default_factory=uuid7, primary_key=True, index=True)
    prices: list["Price"] = Relationship(back_populates="currency")
    accounts: list["Account"] = Relationship(back_populates="currency")
