from datetime import datetime
from decimal import Decimal
from typing import TYPE_CHECKING
from uuid import UUID

from sqlmodel import Field, Relationship, SQLModel
from uuid6 import uuid7

if TYPE_CHECKING:
    from pensive_shaw.models import Currency


class Price(SQLModel, table=True):
    """Represents the price of a particular currency at a certain time."""

    id: UUID = Field(default_factory=uuid7, primary_key=True, index=True)
    amount: Decimal
    currency_id: UUID = Field(foreign_key="currency.id", index=True)
    currency: "Currency" = Relationship(back_populates="prices")
    created_at: datetime = Field(default_factory=datetime.now)
