from fastapi import FastAPI
from pensive_shaw.api import router as api_router

app = FastAPI(
    title="Pensive Shaw", description="A personal finance API", version="0.0.1"
)

app.include_router(api_router)
