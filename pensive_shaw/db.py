import os

import structlog
from sqlmodel import Session, create_engine

slogger = structlog.get_logger(__name__)

DB_URL = (
    (
        f"postgresql+psycopg://{os.environ['POSTGRES_USER']}:{os.environ['POSTGRES_PASSWORD']}"
        f"@{os.environ['DB_HOST']}:5432/{os.environ['POSTGRES_DB']}"
    )
    if os.environ["ENV"] != "test"
    else "postgresql+psycopg://user:pass@host:1111/db"
)

engine = create_engine(DB_URL, echo=True)


def get_session():
    with Session(engine) as session:
        session.begin()
        try:
            yield session
            session.commit()
        except Exception as e:
            slogger.error(
                "Error with database. Rolling back transaction.", error=str(e)
            )
            session.rollback()
            raise
