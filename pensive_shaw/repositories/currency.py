from fastapi import Depends
from pensive_shaw.db import get_session
from pensive_shaw.models import Currency
from sqlmodel import Session, select
from sqlalchemy.exc import NoResultFound


class CurrencyRepository:
    """Repository class for Currency Entity"""

    session = Session

    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def list(self) -> list[Currency]:
        return self.session.exec(select(Currency)).all()

    def get(self, currency: Currency) -> Currency:
        try:
            return self.session.get_one(Currency, currency.id)
        except NoResultFound as e:
            raise ValueError(f"No Currency with ID {currency.id} found.") from e

    def create(self, currency: Currency) -> Currency:
        self.session.add(currency)
        self.session.flush()
        self.session.refresh(currency)
        return currency

    def update(self, currency: Currency) -> Currency:
        self.session.merge(currency)
        self.session.flush()
        return self.get(currency)

    def delete(self, currency: Currency) -> None:
        self.session.delete(currency)
        self.session.flush()
