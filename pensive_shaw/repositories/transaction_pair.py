from uuid import UUID

from fastapi import Depends
from pensive_shaw.db import get_session
from pensive_shaw.models import TransactionPair
from sqlmodel import Session, select, or_


class TransactionPairRepository:
    """Repository class for TransactionPair Entity"""

    session = Session

    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def list(self, txn_ids: list[UUID] | None) -> list[TransactionPair]:
        stmt = select(TransactionPair)

        if txn_ids:
            stmt = stmt.filter(
                or_(
                    TransactionPair.debit_txn_id.in_(txn_ids),
                    TransactionPair.credit_txn_id.in_(txn_ids),
                )
            )

        return self.session.exec(stmt).all()

    def get(self, txn_pair: TransactionPair) -> TransactionPair:
        return self.session.get_one(TransactionPair, txn_pair.id)

    def create(self, txn_pair: TransactionPair) -> TransactionPair:
        self.session.add(txn_pair)
        self.session.flush()
        self.session.refresh(txn_pair)
        return txn_pair

    def update(self, txn_pair: TransactionPair) -> TransactionPair:
        self.session.merge(txn_pair)
        self.session.flush()
        return txn_pair

    def delete(self, txn_pair: TransactionPair) -> None:
        self.session.delete(txn_pair)
        self.session.flush()
