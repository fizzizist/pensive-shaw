from fastapi import Depends
from pensive_shaw.db import get_session
from pensive_shaw.models import Book
from sqlmodel import Session, select


class BookRepository:
    """Repository class for Book Entity"""

    session = Session

    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def list(self) -> list[Book]:
        stmt = select(Book)
        return self.session.exec(stmt).all()

    def get(self, book: Book) -> Book:
        return self.session.get_one(Book, book.id)

    def create(self, book: Book) -> Book:
        self.session.add(book)
        self.session.flush()
        self.session.refresh(book)
        return book

    def update(self, book: Book) -> Book:
        self.session.merge(book)
        self.session.flush()
        return book

    def delete(self, book: Book) -> None:
        self.session.delete(book)
        self.session.flush()
