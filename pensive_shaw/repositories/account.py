from fastapi import Depends
from pensive_shaw.db import get_session
from pensive_shaw.models import Account
from sqlalchemy.exc import DataError, NoResultFound
from sqlmodel import Session, select


class AccountRepository:
    """Repository class for Account Entity"""

    session = Session

    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def list(self) -> list[Account]:
        return self.session.exec(select(Account)).all()

    def get(self, acc: Account) -> Account:
        try:
            return self.session.get_one(Account, acc.id)
        except NoResultFound as e:
            raise ValueError(f"No Account with ID {acc.id} found.") from e

    def create(self, acc: Account) -> Account:
        try:
            self.session.add(acc)
            self.session.flush()
        except DataError as e:
            raise ValueError(e) from e
        self.session.refresh(acc)
        return acc

    def update(self, acc: Account) -> Account:
        self.session.merge(acc)
        self.session.flush()
        return self.get(acc)

    def delete(self, acc: Account) -> None:
        self.session.delete(acc)
        self.session.flush()
