from uuid import UUID

from fastapi import Depends
from pensive_shaw.db import get_session
from pensive_shaw.models.transaction import Transaction, TransactionType
from sqlalchemy.sql import text
from sqlalchemy.exc import NoResultFound
from sqlmodel import Session, select
from typing import List


class TransactionRepository:
    """Repository class for Transaction Entity"""

    session = Session

    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def list(
        self,
        txn_type: TransactionType | None,
        acc_id: UUID | None,
        ids: list[UUID] | None,
    ) -> list[Transaction]:
        query = select(Transaction)

        if txn_type:
            query = query.filter_by(txn_type=txn_type)

        if acc_id:
            query = query.filter_by(account_id=acc_id)

        if ids:
            query = query.filter(Transaction.id.in_(ids))

        return self.session.exec(query).all()

    def get(self, txn: Transaction) -> Transaction:
        try:
            return self.session.get_one(Transaction, txn.id)
        except NoResultFound as e:
            raise ValueError(f"Transaction with ID {txn.id} not found.") from e

    def create(self, txn: Transaction) -> Transaction:
        self.session.add(txn)
        self.session.flush()
        self.session.refresh(txn)
        return txn

    def create_many(self, txns: List[Transaction]) -> List[Transaction]:
        self.session.add_all(txns)
        self.session.flush()
        for txn in txns:
            self.session.refresh(txn)
        return txns

    def system_check(self) -> bool:
        """The sum of all debits must match the sum of all credits

        Note: Using raw SQL statements here as this op might get slow in future.
        """
        if not self.session.exec(
            text("SELECT EXISTS(SELECT id FROM transaction);")
        ).scalar():
            # if no transactions exist yet, system is healthy
            return True
        query = text("""
            SELECT
                (SELECT SUM(amount) FROM transaction WHERE txn_type = 'DEBIT') =
                (SELECT SUM(amount) FROM transaction WHERE txn_type = 'CREDIT')
            ;
        """)
        return self.session.execute(query).scalar()

    def update(self, txn: Transaction) -> Transaction:
        self.session.merge(txn)
        self.session.flush()
        return txn

    def delete(self, txn: Transaction) -> None:
        self.session.delete(txn)
        self.session.flush()

    def delete_many(self, txns: List[Transaction]) -> None:
        for txn in txns:
            self.session.delete(txn)
        self.session.flush()
