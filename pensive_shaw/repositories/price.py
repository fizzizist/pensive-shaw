from uuid import UUID

from fastapi import Depends
from pensive_shaw.db import get_session
from pensive_shaw.models import Price
from sqlmodel import Session, select


class PriceRepository:
    """Repository class for Price Entity"""

    session = Session

    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def list(self) -> list[Price]:
        return self.session.exec(select(Price)).all()

    def get(self, price: Price) -> Price:
        return self.session.get_one(price, price.id)

    def create(self, price: Price) -> Price:
        self.session.add(price)
        self.session.flush()
        self.session.refresh(price)
        return price

    def update(self, id: UUID, price: Price) -> Price:
        price.id = id
        self.session.merge(price)
        self.session.flush()
        return price

    def delete(self, price: Price) -> None:
        self.session.delete(price)
        self.session.flush()
