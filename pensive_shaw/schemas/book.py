from pydantic import BaseModel


class BookInput(BaseModel):
    name: str
