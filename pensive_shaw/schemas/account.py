from uuid import UUID

from pensive_shaw.models.account import AccountBase, AccountType
from pensive_shaw.schemas.currency import CurrencyOutput


class AccountInput(AccountBase):
    pass


class AccountUpdate(AccountBase):
    id: UUID
    name: str | None = None
    currency_id: UUID | None = None
    book_id: UUID | None = None
    acc_type: AccountType | None = None


class AccountOutput(AccountBase):
    id: UUID
    currency: CurrencyOutput
