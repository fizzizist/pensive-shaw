from decimal import Decimal
from uuid import UUID

from pydantic import BaseModel


class PriceInput(BaseModel):
    currency_id: UUID
    amount: Decimal
