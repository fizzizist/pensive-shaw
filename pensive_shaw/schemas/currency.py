from pensive_shaw.models.currency import CurrencyBase, CurrencyType
from uuid import UUID


class CurrencyInput(CurrencyBase):
    pass


class CurrencyOutput(CurrencyBase):
    id: UUID


class CurrencyUpdate(CurrencyBase):
    id: UUID
    symbol: str | None = None
    currency_type: CurrencyType | None = None
    name: str | None = None
    description: str | None = None
