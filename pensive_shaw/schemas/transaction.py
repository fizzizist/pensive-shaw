from datetime import date
from decimal import Decimal
from uuid import UUID

from pensive_shaw.models.transaction import TransactionBase, TransactionType
from pensive_shaw.models.transaction_pair import TransactionPairBase
from pydantic import BaseModel


class TransactionInput(BaseModel):
    credit_account_id: UUID
    debit_account_id: UUID
    amount: Decimal
    currency_id: UUID


class TransactionOutput(TransactionBase):
    pass


class TransactionPairOutput(TransactionPairBase):
    debit_txn: TransactionOutput
    credit_txn: TransactionOutput


class TransactionUpdate(BaseModel):
    id: UUID
    description: str | None = None
    paired_account_id: UUID | None = None
    amount: Decimal | None = None
    txn_type: TransactionType | None = None
    happened_on: date | None = None
    reconciled: bool | None = None
