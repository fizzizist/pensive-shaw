from uuid import UUID

from fastapi import Depends
from pensive_shaw.models.book import Book
from pensive_shaw.repositories.book import BookRepository
from pensive_shaw.schemas.book import BookInput


class BookService:
    """Service for handling operations on Books"""

    book_repo: BookRepository

    def __init__(
        self,
        book_repo: BookRepository = Depends(),
    ):
        self.book_repo = book_repo

    def get(self, book_id: UUID) -> Book:
        return self.book_repo.get(Book(id=book_id))

    def list(self) -> list[Book]:
        return self.book_repo.list()

    def create(self, book_input: BookInput) -> Book:
        return self.book_repo.create(Book(**book_input.model_dump()))

    def delete(self, book: Book) -> None:
        return self.book_repo.delete(book)

    def update(self, book: Book) -> Book:
        return self.book_repo.update(book)
