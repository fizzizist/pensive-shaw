from uuid import UUID

from fastapi import Depends
from pensive_shaw.models.price import Price
from pensive_shaw.repositories.price import PriceRepository
from pensive_shaw.schemas.price import PriceInput


class PriceService:
    """Service for handling operations on Prices"""

    price_repo: PriceRepository

    def __init__(
        self,
        price_repo: PriceRepository = Depends(),
    ):
        self.price_repo = price_repo

    def get(self, price_id: UUID) -> Price:
        return self.price_repo.get(Price(id=price_id))

    def list(self) -> list[Price]:
        return self.price_repo.list()

    def create(self, price_input: PriceInput) -> Price:
        return self.price_repo.create(Price(**PriceInput))

    def delete(self, price: Price) -> None:
        return self.price_repo.delete(price)

    def update(self, price: Price) -> Price:
        return self.price_repo.update(price)
