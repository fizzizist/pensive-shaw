from uuid import UUID

from fastapi import Depends
from pensive_shaw.models.transaction import Transaction, TransactionType
from pensive_shaw.models.transaction_pair import TransactionPair
from pensive_shaw.repositories.transaction import TransactionRepository
from pensive_shaw.repositories.transaction_pair import TransactionPairRepository
from pensive_shaw.schemas.transaction import TransactionInput, TransactionUpdate


class TransactionService:
    """Service for handling operations on Transactions"""

    txn_repo: TransactionRepository
    txn_pair_repo: TransactionPairRepository

    def __init__(
        self,
        txn_repo: TransactionRepository = Depends(),
        txn_pair_repo: TransactionPairRepository = Depends(),
    ):
        self.txn_repo = txn_repo
        self.txn_pair_repo = txn_pair_repo

    def system_check(self) -> None:
        healthy = self.txn_repo.system_check()
        if not healthy:
            raise ValueError("The database is corrupted. Please rebalance transactions")

    def get(self, txn_id: UUID) -> Transaction:
        return self.txn_repo.get(Transaction(id=txn_id))

    def list(
        self,
        txn_type: TransactionType | None = None,
        acc_id: UUID | None = None,
        ids: list[UUID] | None = None,
    ) -> list[Transaction]:
        return self.txn_repo.list(txn_type=txn_type, acc_id=acc_id, ids=ids)

    def create(self, txn_input: TransactionInput) -> TransactionPair:
        credit_txn = self.txn_repo.create(
            Transaction(
                account_id=txn_input.credit_account_id,
                paired_account_id=txn_input.debit_account_id,
                amount=txn_input.amount,
                txn_type=TransactionType.CREDIT,
            )
        )
        debit_txn = self.txn_repo.create(
            Transaction(
                account_id=txn_input.debit_account_id,
                paired_account_id=txn_input.credit_account_id,
                amount=txn_input.amount,
                txn_type=TransactionType.DEBIT,
            )
        )

        if not (
            credit_txn.account.currency_id
            == debit_txn.account.currency_id
            == txn_input.currency_id
        ):
            raise ValueError(
                "Invalid transaction. Both accounts must have the same currency."
            )

        txn_pair = self.txn_pair_repo.create(
            TransactionPair(debit_txn_id=debit_txn.id, credit_txn_id=credit_txn.id)
        )
        self.system_check()
        return txn_pair

    def delete(self, txn_id: UUID) -> None:
        try:
            txn_pair = self.txn_pair_repo.list(txn_ids=[txn_id])[0]
        except IndexError as e:
            raise ValueError(
                f"No Transaction Pairs exist that contain transaction ID {txn_id}"
            ) from e
        self.txn_pair_repo.delete(txn_pair)
        self.txn_repo.delete(txn_pair.debit_txn)
        self.txn_repo.delete(txn_pair.credit_txn)
        self.system_check()

    def update(self, txn_update: TransactionUpdate) -> TransactionPair:
        """Updates a transaction. Some operations cannot be performed with
        an update and others have to be done to both transactions in the pair.
        """
        txn_pair = self.txn_pair_repo.list(txn_ids=[txn_update.id])[0]
        if txn_pair.debit_txn.id == txn_update.id:
            this_txn = txn_pair.debit_txn
            paired_txn = txn_pair.credit_txn
        else:
            this_txn = txn_pair.credit_txn
            paired_txn = txn_pair.debit_txn

        if txn_update.paired_account_id:
            this_txn.paired_account_id = txn_update.paired_account_id
            paired_txn.account_id = txn_update.paired_account_id

        if txn_update.amount:
            this_txn.amount = txn_update.amount
            paired_txn.amount = txn_update.amount

        if txn_update.txn_type:
            this_txn.txn_type = txn_update.txn_type
            paired_txn.txn_type = (
                TransactionType.DEBIT
                if this_txn.txn_type == TransactionType.CREDIT
                else TransactionType.CREDIT
            )
            txn_pair.debit_txn = (
                this_txn if this_txn.txn_type == TransactionType.DEBIT else paired_txn
            )
            txn_pair.credit_txn = (
                this_txn if this_txn.txn_type == TransactionType.CREDIT else paired_txn
            )

        if txn_update.description:
            this_txn.description = txn_update.description
            paired_txn.description = txn_update.description

        if txn_update.happened_on:
            this_txn.happened_on = txn_update.happened_on
            paired_txn.happened_on = txn_update.happened_on

        if txn_update.reconciled:
            this_txn.reconciled = txn_update.reconciled
            paired_txn.reconciled = txn_update.reconciled

        self.txn_repo.update(this_txn)
        self.txn_repo.update(paired_txn)
        txn_pair = self.txn_pair_repo.update(txn_pair)
        self.system_check()
        return txn_pair
