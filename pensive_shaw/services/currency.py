from uuid import UUID

from fastapi import Depends
from pensive_shaw.models import Currency
from pensive_shaw.repositories.currency import CurrencyRepository
from pensive_shaw.repositories.price import PriceRepository
from pensive_shaw.schemas.currency import CurrencyInput, CurrencyUpdate


class CurrencyService:
    """Service for handling operations on Currencys"""

    currency_repo: CurrencyRepository
    price_repo: PriceRepository

    def __init__(
        self,
        currency_repo: CurrencyRepository = Depends(),
        price_repo: PriceRepository = Depends(),
    ):
        self.currency_repo = currency_repo
        self.price_repo = price_repo

    def get(self, currency_id: UUID) -> Currency:
        return self.currency_repo.get(Currency(id=currency_id))

    def list(self) -> list[Currency]:
        return self.currency_repo.list()

    def create(self, currency_input: CurrencyInput) -> Currency:
        curr = self.currency_repo.create(Currency(**currency_input.model_dump()))
        return curr

    def delete(self, currency: Currency) -> None:
        return self.currency_repo.delete(currency)

    def update(self, currency: CurrencyUpdate) -> Currency:
        # try to get, which will throw error if not.
        self.currency_repo.get(Currency(id=currency.id))
        return self.currency_repo.update(
            Currency(**currency.model_dump(exclude_unset=True))
        )
