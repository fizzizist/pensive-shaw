from uuid import UUID

from fastapi import Depends
from pensive_shaw.models.account import Account
from pensive_shaw.repositories.account import AccountRepository
from pensive_shaw.schemas.account import AccountInput, AccountUpdate


class AccountService:
    """Service for handling operations on Accounts"""

    acc_repo: AccountRepository

    def __init__(
        self,
        acc_repo: AccountRepository = Depends(),
    ):
        self.acc_repo = acc_repo

    def get(self, acc_id: UUID) -> Account:
        return self.acc_repo.get(Account(id=acc_id))

    def list(self) -> list[Account]:
        return self.acc_repo.list()

    def create(self, acc_input: AccountInput) -> Account:
        return self.acc_repo.create(Account(**acc_input.model_dump()))

    def delete(self, acc: Account) -> None:
        return self.acc_repo.delete(acc)

    def update(self, acc: AccountUpdate) -> Account:
        self.acc_repo.get(Account(id=acc.id))
        return self.acc_repo.update(Account(**acc.model_dump(exclude_unset=True)))
