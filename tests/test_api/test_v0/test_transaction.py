from datetime import date
from decimal import Decimal

import pytest
from pensive_shaw.models.account import AccountType
from pensive_shaw.models.currency import CurrencyType
from pensive_shaw.models.transaction import TransactionType
from pensive_shaw.repositories.transaction import TransactionRepository
from pensive_shaw.repositories.transaction_pair import TransactionPairRepository
from tests.model_factories import (
    AccountFactory,
    CurrencyFactory,
    TransactionFactory,
    TransactionPairFactory,
)
from uuid6 import uuid7


def test_transaction_post(test_client, db_session):
    cad = CurrencyFactory(symbol="CAD", currency_type=CurrencyType.WORLD)
    ass_account = AccountFactory(acc_type=AccountType.ASSET, currency=cad)
    lib_account = AccountFactory(acc_type=AccountType.LIABILITY, currency=cad)

    # the liability account is transferring 129.37 CAD to the asset account
    data = {
        "credit_account_id": str(ass_account.id),
        "debit_account_id": str(lib_account.id),
        "amount": 129.37,
        "currency_id": str(cad.id),
    }

    resp = test_client.post("/api/v0/transactions", json=data)
    assert resp.status_code == 201
    txn_pair = resp.json()
    assert txn_pair["debit_txn"]["amount"] == txn_pair["credit_txn"]["amount"]
    assert txn_pair["debit_txn"]["txn_type"] == "DEBIT"
    assert txn_pair["credit_txn"]["txn_type"] == "CREDIT"
    assert txn_pair["debit_txn"]["reconciled"] is False
    assert txn_pair["credit_txn"]["reconciled"] is False


def test_transaction_post_422(test_client, db_session):
    cad = CurrencyFactory(symbol="CAD", currency_type=CurrencyType.WORLD)
    ass_account = AccountFactory(acc_type=AccountType.ASSET)
    lib_account = AccountFactory(acc_type=AccountType.LIABILITY)

    # the liability account is transferring 129.37 CAD to the asset account
    data = {
        "credit_account_id": str(ass_account.id),
        "debit_account_id": str(lib_account.id),
        "amount": 129.37,
        "currency_id": str(cad.id),
    }

    resp = test_client.post("/api/v0/transactions", json=data)
    assert resp.status_code == 422
    assert len(TransactionRepository(session=db_session).list(None, None, None)) == 0


@pytest.mark.parametrize(
    "filters,result_count",
    [
        ({"txn-type": "DEBIT"}, 4),
        ({"account-id": "0190425f-ff1a-7232-b100-8bd0c16b0cf0"}, 3),
        (
            {
                "ids-in": [
                    "01904261-3049-7f87-9cc1-58428a210375",
                    "01904261-3a95-73ea-ab81-1ad5bf31196e",
                ]
            },
            2,
        ),
        (
            {
                "account-id": "0190425f-ff1a-7232-b100-8bd0c16b0cf0",
                "txn-type": "CREDIT",
            },
            1,
        ),
    ],
)
def test_transaction_list(test_client, db_session, filters, result_count):
    acc = AccountFactory(id="0190425f-ff1a-7232-b100-8bd0c16b0cf0")
    TransactionFactory(
        id="01904261-3049-7f87-9cc1-58428a210375",
        account=acc,
        txn_type=TransactionType.DEBIT,
    )
    TransactionFactory(account=acc, txn_type=TransactionType.CREDIT)
    TransactionFactory(account=acc, txn_type=TransactionType.DEBIT)
    TransactionFactory(
        id="01904261-3a95-73ea-ab81-1ad5bf31196e", txn_type=TransactionType.DEBIT
    )
    TransactionFactory(txn_type=TransactionType.DEBIT)

    # no filter
    resp = test_client.get("/api/v0/transactions", params=filters)
    assert resp.status_code == 200
    assert len(resp.json()) == result_count


def test_transaction_get(test_client):
    txn = TransactionFactory()
    resp = test_client.get(f"/api/v0/transactions/{txn.id}")
    assert resp.status_code == 200
    assert str(txn.id) == resp.json()["id"]


def test_transaction_get_404(test_client):
    resp = test_client.get(f"/api/v0/transactions/{uuid7()}")
    assert resp.status_code == 404


def test_transaction_delete_404(test_client):
    resp = test_client.delete(f"/api/v0/transactions/{uuid7()}")
    assert resp.status_code == 404


def test_transaction_delete(test_client, db_session):
    txn_pair = TransactionPairFactory()
    resp = test_client.delete(f"/api/v0/transactions/{txn_pair.debit_txn.id}")
    assert resp.status_code == 204
    repo = TransactionRepository(session=db_session)
    assert len(repo.list(None, None, None)) == 0
    repo = TransactionPairRepository(session=db_session)
    assert len(repo.list(None)) == 0


# update amount
def test_transaction_update_amount(test_client, db_session):
    debit = TransactionFactory(amount=Decimal("123.35"), txn_type=TransactionType.DEBIT)
    credit = TransactionFactory(
        amount=Decimal("123.35"), txn_type=TransactionType.CREDIT
    )
    txn_pair = TransactionPairFactory(debit_txn=debit, credit_txn=credit)

    data = {"id": str(debit.id), "amount": "453.45"}

    resp = test_client.patch("/api/v0/transactions", json=data)
    assert resp.status_code == 200
    # refresh after update
    txn_pair = TransactionPairRepository(session=db_session).get(txn_pair)
    assert txn_pair.debit_txn.amount == Decimal("453.45")
    assert txn_pair.credit_txn.amount == Decimal("453.45")


# update account
def test_transaction_update_account(test_client, db_session):
    other_acc = AccountFactory()
    debit = TransactionFactory(txn_type=TransactionType.DEBIT)
    credit = TransactionFactory(txn_type=TransactionType.CREDIT)
    txn_pair = TransactionPairFactory(debit_txn=debit, credit_txn=credit)

    data = {"id": str(debit.id), "paired_account_id": str(other_acc.id)}

    resp = test_client.patch("/api/v0/transactions", json=data)
    assert resp.status_code == 200
    # refresh after update
    txn_pair = TransactionPairRepository(session=db_session).get(txn_pair)
    assert txn_pair.credit_txn.account_id == other_acc.id


# update txn type
def test_transaction_update_txn_type(test_client, db_session):
    debit = TransactionFactory(txn_type=TransactionType.DEBIT)
    credit = TransactionFactory(txn_type=TransactionType.CREDIT)
    txn_pair = TransactionPairFactory(debit_txn=debit, credit_txn=credit)

    data = {"id": str(debit.id), "txn_type": "CREDIT"}

    resp = test_client.patch("/api/v0/transactions", json=data)
    assert resp.status_code == 200
    # refresh after update
    txn_pair = TransactionPairRepository(session=db_session).get(txn_pair)
    assert txn_pair.debit_txn.id == credit.id
    assert txn_pair.debit_txn.txn_type == TransactionType.DEBIT
    assert txn_pair.credit_txn.id == debit.id
    assert txn_pair.credit_txn.txn_type == TransactionType.CREDIT


# reconcile
def test_transaction_update_reconciled(test_client, db_session):
    txn_pair = TransactionPairFactory()

    data = {"id": str(txn_pair.debit_txn.id), "reconciled": True}

    resp = test_client.patch("/api/v0/transactions", json=data)
    assert resp.status_code == 200
    # refresh after update
    txn_pair = TransactionPairRepository(session=db_session).get(txn_pair)
    assert txn_pair.debit_txn.reconciled
    assert txn_pair.credit_txn.reconciled


def test_transaction_update_description(test_client, db_session):
    txn_pair = TransactionPairFactory()

    data = {"id": str(txn_pair.debit_txn.id), "description": "Sent money to my Mom"}

    resp = test_client.patch("/api/v0/transactions", json=data)
    assert resp.status_code == 200
    # refresh after update
    txn_pair = TransactionPairRepository(session=db_session).get(txn_pair)
    assert txn_pair.debit_txn.description == "Sent money to my Mom"
    assert txn_pair.credit_txn.description == "Sent money to my Mom"


# update date
def test_transaction_update_date(test_client, db_session):
    txn_pair = TransactionPairFactory()

    new_dt = date(2024, 6, 29)

    assert new_dt != txn_pair.debit_txn.happened_on

    data = {"id": str(txn_pair.debit_txn.id), "happened_on": new_dt.isoformat()}

    resp = test_client.patch("/api/v0/transactions", json=data)
    assert resp.status_code == 200
    # refresh after update
    txn_pair = TransactionPairRepository(session=db_session).get(txn_pair)
    assert txn_pair.debit_txn.happened_on == new_dt
    assert txn_pair.credit_txn.happened_on == new_dt
