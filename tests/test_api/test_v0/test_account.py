from tests.model_factories import AccountFactory, BookFactory, CurrencyFactory
from uuid6 import uuid7


def test_account_post(test_client, db_session):
    book = BookFactory()
    currency = CurrencyFactory(symbol="CAD")
    data = {
        "book_id": str(book.id),
        "name": "checking",
        "description": "My first checking account",
        "currency_id": str(currency.id),
        "acc_type": "ASSET",
    }
    resp = test_client.post("/api/v0/accounts", json=data)
    assert resp.status_code == 201
    new_acc = resp.json()
    assert new_acc["currency"]["symbol"] == "CAD"
    assert new_acc["name"] == "checking"
    assert new_acc["acc_type"] == "ASSET"


def test_account_post_422(test_client, db_session):
    book = BookFactory()
    currency = CurrencyFactory(symbol="CAD")
    data = {
        "book_id": str(book.id),
        "name": "checking",
        "description": "My first checking account",
        "currency_id": str(currency.id),
        "acc_type": "ONETUH",  # send some garbled data
    }
    resp = test_client.post("/api/v0/accounts", json=data)
    assert resp.status_code == 422


def test_account_list(test_client, db_session):
    acc = AccountFactory()
    resp = test_client.get("/api/v0/accounts")
    assert resp.status_code == 200
    accs = resp.json()
    assert str(acc.id) == accs[0]["id"]


def test_account_get(test_client, db_session):
    acc = AccountFactory()
    resp = test_client.get(f"/api/v0/accounts/{acc.id}")
    assert resp.status_code == 200
    acc_json = resp.json()
    assert str(acc.id) == acc_json["id"]


def test_account_get_404(test_client, db_session):
    resp = test_client.get(f"/api/v0/accounts/{uuid7()}")
    assert resp.status_code == 404


def test_patch_account(test_client, db_session):
    sub_acc = AccountFactory()
    parent_acc = AccountFactory()
    payload = {
        "id": str(sub_acc.id),
        "parent_id": str(parent_acc.id),
    }
    resp = test_client.patch("/api/v0/accounts", json=payload)
    assert resp.status_code == 200
    updated_acc = resp.json()
    assert str(sub_acc.id) == updated_acc["id"]
    assert updated_acc["parent_id"] == str(parent_acc.id)


def test_patch_account_404(test_client, db_session):
    parent_acc = AccountFactory()
    payload = {
        "id": str(uuid7()),
        "parent_id": str(parent_acc.id),
    }
    resp = test_client.patch("/api/v0/accounts", json=payload)
    assert resp.status_code == 404
