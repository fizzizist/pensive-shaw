from pensive_shaw.repositories.currency import CurrencyRepository
from tests.model_factories import CurrencyFactory
from uuid6 import uuid7


def test_currency_post(test_client, db_session):
    data = {
        "symbol": "USD",
        "currency_type": "WORLD",
        "name": "United States Dollar",
        "description": "The official currency of the United States.",
    }
    resp = test_client.post("/api/v0/currencies", json=data)
    assert resp.status_code == 201
    assert len(CurrencyRepository(session=db_session).list()) == 1


def test_currency_patch(test_client, db_session):
    curr = CurrencyFactory(
        symbol="BTC",
        currency_type="CRYPTO",
        name="BitCoin",
        description="The first cryptocurrency ever.",
    )
    data = {
        "id": str(curr.id),
        "symbol": "USD",
        "currency_type": "WORLD",
        "name": "United States Dollar",
        "description": "The official currency of the United States.",
    }
    resp = test_client.patch("/api/v0/currencies", json=data)
    assert resp.status_code == 200
    updated = resp.json()
    for key in data:
        assert updated[key] == data[key]


def test_currency_patch_subset(test_client, db_session):
    curr = CurrencyFactory(
        symbol="BTC",
        currency_type="CRYPTO",
        name="BitCoin",
        description="The first cryptocurrency ever.",
    )
    data = {
        "id": str(curr.id),
        "currency_type": "WORLD",
        "name": "United States Dollar",
        "description": None,
    }
    resp = test_client.patch("/api/v0/currencies", json=data)
    assert resp.status_code == 200
    updated = resp.json()
    assert updated["symbol"] == "BTC"
    assert updated["currency_type"] == "WORLD"
    assert updated["description"] is None


def test_currency_patch_404(test_client, db_session):
    data = {
        "id": str(uuid7()),
        "symbol": "USD",
        "currency_type": "WORLD",
        "name": "United States Dollar",
        "description": "The official currency of the United States.",
    }
    resp = test_client.patch("/api/v0/currencies", json=data)
    assert resp.status_code == 404


def test_currency_list(test_client, db_session):
    CurrencyFactory()
    CurrencyFactory()
    resp = test_client.get("/api/v0/currencies")
    assert resp.status_code == 200
    assert len(resp.json()) == 2


def test_currency_get(test_client, db_session):
    curr = CurrencyFactory()
    resp = test_client.get(f"/api/v0/currencies/{curr.id}")
    assert resp.status_code == 200
    assert resp.json()["id"] == str(curr.id)


def test_currency_get_404(test_client, db_session):
    resp = test_client.get(f"/api/v0/currencies/{uuid7()}")
    assert resp.status_code == 404
