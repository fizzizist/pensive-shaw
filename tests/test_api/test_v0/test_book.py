from pensive_shaw.repositories.book import BookRepository
from tests.model_factories import BookFactory


def test_post_book(test_client, db_session):
    payload = {"name": "Personal Finances"}
    resp = test_client.post("/api/v0/books", json=payload)
    assert resp.status_code == 201
    assert len(BookRepository(session=db_session).list()) == 1


def test_patch_book(test_client, db_session):
    book = BookFactory(name="Personal Finance")
    payload = {"id": str(book.id), "name": "Personal Finances v1"}
    resp = test_client.patch("/api/v0/books", json=payload)
    assert resp.status_code == 200
    book_json = resp.json()
    assert str(book.id) == book_json["id"]
    assert book_json["name"] == payload["name"]


def test_list_book(test_client, db_session):
    book = BookFactory()
    resp = test_client.get("/api/v0/books")
    assert resp.status_code == 200
    assert resp.json()[0]["id"] == str(book.id)


def test_get_book(test_client, db_session):
    book = BookFactory()
    resp = test_client.get(f"/api/v0/books/{book.id}")
    assert resp.status_code == 200
    assert resp.json()["id"] == str(book.id)
