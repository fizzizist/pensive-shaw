from decimal import Decimal

import factory
from pensive_shaw.models import Book, TransactionPair
from pensive_shaw.models.account import Account, AccountType
from pensive_shaw.models.currency import Currency, CurrencyType
from pensive_shaw.models.transaction import Transaction, TransactionType


class BookFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Book

    name = factory.Sequence(lambda n: f"book{n}")


class CurrencyFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Currency

    currency_type = CurrencyType.WORLD
    symbol = factory.Sequence(lambda n: f"CAD{n}")


class AccountFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Account

    name = "fake account"
    currency = factory.SubFactory(CurrencyFactory)
    book = factory.SubFactory(BookFactory)
    acc_type = AccountType.ASSET


class TransactionFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Transaction

    account = factory.SubFactory(AccountFactory)
    paired_account = factory.SubFactory(AccountFactory)
    amount = Decimal("32453.34")
    txn_type = TransactionType.CREDIT


class TransactionPairFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = TransactionPair

    debit_txn = factory.SubFactory(TransactionFactory, txn_type=TransactionType.DEBIT)
    credit_txn = factory.SubFactory(TransactionFactory, txn_type=TransactionType.CREDIT)


FACTORY_MODELS = [
    BookFactory,
    CurrencyFactory,
    AccountFactory,
    TransactionFactory,
    TransactionPairFactory,
]
