import contextlib

import pytest
import testing.postgresql
from fastapi.testclient import TestClient
from pensive_shaw.app import app
from pensive_shaw.db import get_session
from sqlmodel import Session, SQLModel, create_engine
from tests.model_factories import FACTORY_MODELS


@pytest.fixture
def test_client(db_session):
    def get_test_session():
        db_session.begin_nested()
        try:
            yield db_session
        except Exception:
            db_session.rollback()
            raise

    app.dependency_overrides[get_session] = get_test_session
    client = TestClient(app)
    yield client


@pytest.fixture(scope="session")
def postgresql():
    p = testing.postgresql.Postgresql()

    yield p

    p.stop()


@pytest.fixture(scope="session")
def db_engine(postgresql):
    engine = create_engine(postgresql.url().replace("postgresql", "postgresql+psycopg"))
    SQLModel.metadata.create_all(engine)

    yield engine


@pytest.fixture(autouse=True)
def db_session(db_engine):
    with contextlib.closing(db_engine.connect()) as conn:
        with Session(conn) as session:
            for m in FACTORY_MODELS:
                m._meta.sqlalchemy_session = session

            trans = conn.begin()
            try:
                yield session
            finally:
                if trans.is_active:
                    trans.rollback()
