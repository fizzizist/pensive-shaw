FROM python:3.12-bookworm AS base
ADD --chmod=755 https://astral.sh/uv/install.sh /install.sh
RUN /install.sh && rm /install.sh

FROM base AS dev

WORKDIR /app
COPY requirements/dev.txt /app/requirements.txt
RUN /root/.cargo/bin/uv pip install --system --no-cache -r requirements.txt

FROM base AS test

RUN apt update
RUN apt -y install postgresql

COPY requirements/dev.txt /requirements.txt
RUN /root/.cargo/bin/uv pip install --system --no-cache -r requirements.txt

RUN useradd -ms /bin/bash pensive
WORKDIR /home/pensive
USER pensive

ENTRYPOINT ["pytest", "--cov=pensive_shaw", "--cov-fail-under=80"]
